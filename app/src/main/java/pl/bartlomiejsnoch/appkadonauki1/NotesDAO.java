package pl.bartlomiejsnoch.appkadonauki1;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

import java.util.List;

/**
 * Created by bartek on 12.12.2017.
 */

@Dao
public interface NotesDAO {
    @Query("SELECT * FROM notes")
    LiveData<List<Notes>> getall();

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    public void insertNotes(Notes... notes);

    @Delete
    void delete(Notes notes);

    @Query("SELECT noteID, title FROM notes")
    List<Notes> getTitle();

    @Query("SELECT title, content FROM notes")
    List<Notes> getContent();

    @Query("SELECT * FROM notes WHERE noteID = :position")
    List<Notes> getContentByPosition(int position);
}
