package pl.bartlomiejsnoch.appkadonauki1;

import android.view.View;

/**
 * Created by bartek on 26.12.2017.
 * https://piercezaifman.com/click-listener-for-recyclerview-adapter/ <3
 */

public interface RecyclerViewClickListener {
    void onClick(View view, int position);
}
