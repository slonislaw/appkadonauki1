package pl.bartlomiejsnoch.appkadonauki1;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class GoReadKeyValueActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_read_key_value);
        SharedPreferences sharedPref = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);
        int intkeyvalue = sharedPref.getInt("key",  0);
        String stringkeyvalue = Integer.toString(intkeyvalue);
        TextView textView = (TextView) findViewById(R.id.textView4);
        textView.setText(stringkeyvalue);
    }
}
