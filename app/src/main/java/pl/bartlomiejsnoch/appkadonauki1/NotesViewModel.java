package pl.bartlomiejsnoch.appkadonauki1;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

/**
 * Created by USER on 19.12.2017.
 */


//https://developer.android.com/topic/libraries/architecture/adding-components.html
    //https://android.jlelse.eu/android-architecture-components-room-livedata-and-viewmodel-fca5da39e26b Create the AndroidViewModel
    //lista notatek jest wrapowana przez LiveData co pozwala innym klasom obserwować zmiany danych wewnatrz wrappera
    //tutaj wrappuje liste notatek przez co Activity sledzi zmiany i moze uaktualniac interfejs

public class NotesViewModel extends AndroidViewModel { //kazdy ViewModel musi rozszerzac klase AndroidViewModel

    private final LiveData<List<Notes>> notesList;
    private final List<Notes> content;

    private AppDataBase dataBase;

    public NotesViewModel(Application application) {
        super(application);

        dataBase = AppDataBase.getAppDataBase(this.getApplication());

        notesList = dataBase.notesDAO().getall();

        content = dataBase.notesDAO().getContentByPosition(1);
    }
    public LiveData<List<Notes>> getNotesList() {
        return notesList;
    }

    public List<Notes> getContent() {
        return content;
    }

    public void deleteNote(Notes notes) {
        new deleteAsyncTask(dataBase).execute(notes);

    }



    private static class deleteAsyncTask extends AsyncTask<Notes, Void, Void> {

        private AppDataBase db;

        deleteAsyncTask(AppDataBase appDataBase) {
            db = appDataBase;
        }

        @Override
        protected Void doInBackground(final Notes... params) {
            db.notesDAO().delete(params[0]);
            return null;
        }

    }


}
