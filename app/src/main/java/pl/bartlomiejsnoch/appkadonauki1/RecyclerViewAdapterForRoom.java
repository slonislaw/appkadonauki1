package pl.bartlomiejsnoch.appkadonauki1;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by USER on 19.12.2017.
 */
//Na podstawie:
    //https://android.jlelse.eu/android-architecture-components-room-livedata-and-viewmodel-fca5da39e26b
    //http://damianchodorek.com/kurs-android-lista-siatka-recyclerview-viewholder-cardview-karty-cien-adapter-wzorzec-16/

//adapter to klasa, ktora przechowuje i zarzadza danymi do wyswietlenia!
    // Baza danych -> Adapter -> RecyclerView

    //doczytać: http://blog.iamsuleiman.com/pinterest-masonry-layout-staggered-grid/

public class RecyclerViewAdapterForRoom extends RecyclerView.Adapter<RecyclerViewAdapterForRoom.RecyclerViewHolder> {
    private List<Notes> notesList;
    private RecyclerViewClickListener mListener; // nacisniecie


    public RecyclerViewAdapterForRoom(List<Notes> notesList, RecyclerViewClickListener listener) {
        this.notesList = notesList; //https://javastart.pl/static/programowanie-obiektowe/parametry-i-slowo-kluczowe-this/
        mListener = listener; //??
    }




    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int ViewType) { //RecyclerViewHolder będzie klasą statyczna
        //przechowujaca TextView i ich zawartosci
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_for_room, parent, false);
        return new RecyclerViewHolder(view, mListener);

    }


    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        RecyclerViewClickListener onClickListener;
        Notes notesModel = notesList.get(position);
        holder.titleTextView.setText(notesModel.getTitle());
        holder.dateTextView.setText(notesModel.getDate());
        holder.itemView.setTag(notesModel);//wyjasnic
        String content = notesModel.getContent();
        int noteID = notesModel.getNoteID(); //poglaowo
    }

/* kod ponizej przeniesiony do osobnej klasy RecycerViewAdapterForNoteContent
    @Override
    public RecyclerViewHolderForNotesContent onCreateViewHolderForContent(ViewGroup parent, int ViewType) {
        //dopisać to
        return new RecyclerViewHolderForNotesContent(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_for_notes_content, parent, false));
    }
*/


    @Override
    public int getItemCount() {
        return notesList.size();
    }

    public void addNote(List<Notes> notesList) {
        this.notesList = notesList;
        notifyDataSetChanged();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView titleTextView;
        private TextView dateTextView;
        private RecyclerViewClickListener mListener;


        RecyclerViewHolder(View view, RecyclerViewClickListener listener) {
            super(view);
            titleTextView = (TextView) view.findViewById(R.id.titleTextView);
            dateTextView = (TextView) view.findViewById(R.id.dateTextView);
            mListener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
                mListener.onClick(view, getAdapterPosition());
        }

    }

  /*  static class RecyclerViewHolderForNotesContent extends RecyclerView.ViewHolder {
        private TextView titleTextView;
        private TextView contentTextView;

        RecyclerViewHolderForNotesContent(View view) {
            super(view);
            titleTextView = (TextView) view.findViewById(R.id.titleTextView2);
            contentTextView = (TextView) view.findViewById(R.id.contentTextView);
        } */
    }

