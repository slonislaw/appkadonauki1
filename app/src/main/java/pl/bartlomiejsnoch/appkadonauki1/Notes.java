package pl.bartlomiejsnoch.appkadonauki1;

/**
 * Created by bartek on 12.12.2017.
 */

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity (tableName = "notes")
public class Notes {
    @PrimaryKey(autoGenerate = true)
    public int noteID;

    @ColumnInfo(name = "title")
    public String title;

    @ColumnInfo(name = "content")
    public String content;

    @ColumnInfo(name = "date")
    public String date;

    //https://android.jlelse.eu/android-architecture-components-room-livedata-and-viewmodel-fca5da39e26b - na bazie tego:


    public int getNoteID() {
        return noteID;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getDate() {
        return date;
    }

    public void setNoteTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDate(String date) {
        this.date = date;
    }
}




