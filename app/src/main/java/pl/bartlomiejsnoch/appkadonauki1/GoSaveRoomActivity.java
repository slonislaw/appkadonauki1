package pl.bartlomiejsnoch.appkadonauki1;

import android.arch.persistence.room.Database;
import android.content.Context;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import pl.bartlomiejsnoch.appkadonauki1.AppDataBase;


import java.util.Date;


public class GoSaveRoomActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_save_room);
        populateAsync(AppDataBase.getAppDataBase(this));

    }

    String noteTitleString;  //czy moge te globalne zmienne jakos zastapic funkcja?
    String noteContentString;
    String noteDate;

    public void roomSave(View view) {
        EditText noteTitle = (EditText) findViewById(R.id.editText3);
        EditText noteContent = (EditText) findViewById(R.id.editText4);
        noteTitleString = noteTitle.getText().toString();
        noteContentString = noteContent.getText().toString();
        noteDate = new SimpleDateFormat("ddMMyyyy_HH:mm:ss").format(new Date());
        putNotesData(AppDataBase.getAppDataBase(this)); //dlaczego tu nie działa po prostu putNotesData(dataBase)?
    }

        //https://medium.com/@ajaysaini.official/building-database-with-room-persistence-library-ecf7d0b8f3e9
        //https://stackoverflow.com/questions/17529766/view-contents-of-database-file-in-android-studio - sprawdzilem na podstawie tego i działa!! <3

   public static Notes addNote(final AppDataBase dataBase, Notes notes) {
        dataBase.notesDAO().insertNotes(notes);
        return notes;
    }



    public void putNotesData(AppDataBase dataBase) {
        Notes notes = new Notes();
        notes.setNoteTitle(noteTitleString);
        notes.setContent(noteContentString);
        notes.setDate(noteDate);
        addNote(dataBase, notes);
    }

    public static void populateAsync (@NonNull final AppDataBase dataBase) {
        PopulateDbAsync task = new PopulateDbAsync(dataBase);
        task.execute();
    }

    @Override
    protected void onDestroy() {
        AppDataBase.destroyInstance();
        super.onDestroy();
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> { //doczytac ascync
        private final AppDataBase mDb;
        PopulateDbAsync(AppDataBase dataBase) {
            mDb = dataBase;
        }
        @Override
        protected Void doInBackground(final Void... params) {
          //  populateWithTestData(mDb);
            return null;
        }
    }
}





