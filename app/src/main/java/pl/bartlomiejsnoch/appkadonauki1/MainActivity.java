package pl.bartlomiejsnoch.appkadonauki1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "pl.bartlomiejsnoch.appkadonauki1.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /** Called when the user taps the Send button */
    public void sendMessage(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
    public void goLocale(View view) {
        Intent intent = new Intent(this, GoLocaleActivity.class);
        startActivity(intent);
    }
    public void goFragment(View view) {
        Intent intent = new Intent(this, GoFragmentActivity.class);
        startActivity(intent);
    }
    public void goInteract(View view) {
        Intent intent = new Intent(this, GoInteractActivity.class);
        startActivity(intent);
    }
    public void goShare(View view) {
        Intent intent = new Intent(this, GoShareActivity.class);
        startActivity(intent);
    }
    public void goWrite(View view) {
        Intent intent = new Intent(this, GoWriteKeyValueActivity.class);
        startActivity(intent);
    }
    public void goRead(View view) {
        Intent intent = new Intent(this, GoReadKeyValueActivity.class);
        startActivity(intent);
    }
    public void goSaveFile(View view) {
        Intent intent = new Intent(this, GoSaveFileActivity.class);
        startActivity(intent);
    }
    public void goSaveRoom(View view) {
        Intent intent = new Intent(this, GoSaveRoomActivity.class);
        startActivity(intent);
    }
    public void goReadRoom(View view) {
        Intent intent = new Intent(this, GoReadRoomActivity.class);
        startActivity(intent);
    }
}