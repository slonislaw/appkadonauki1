package pl.bartlomiejsnoch.appkadonauki1;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

/**
 * Created by bartek on 23.12.2017.
 */

public class SingleNoteViewModel extends AndroidViewModel {
    private final List<Notes> noteContent;

    private AppDataBase dataBase;

    public SingleNoteViewModel(Application application) {
        super(application);

        dataBase = AppDataBase.getAppDataBase(this.getApplication());
        noteContent = dataBase.notesDAO().getContent(); // to nie bedzie jeszcze dzialalo, musze dopisac zapytanie w DAO
    }

    public List<Notes> getNoteContent(int position) {
        return noteContent;
    }
}
