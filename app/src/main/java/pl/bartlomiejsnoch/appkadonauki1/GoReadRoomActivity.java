package pl.bartlomiejsnoch.appkadonauki1;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.persistence.room.ColumnInfo;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

//tytuly maja wyswietlac sie na liscie
//po nacisnieciu mam przejsc do konkretnej notatki
//https://javastart.pl/static/programowanie-android/listview-przyklad-widoku-listowego/
//https://android.jlelse.eu/android-architecture-components-room-livedata-and-viewmodel-fca5da39e26b na bazie tego działam z odczytywaniem wpisów z bazy

//z ogromna pomoca: https://piercezaifman.com/click-listener-for-recyclerview-adapter/ w koncu dziala klik i toast wyswietlajacy pozycje!!!


public class GoReadRoomActivity extends AppCompatActivity {

    private NotesViewModel viewModel;
    private RecyclerViewAdapterForRoom recyclerViewAdapterForRoom;
    private RecyclerView recyclerView;

        RecyclerViewClickListener listener = ((view, noteID) -> {
        Toast.makeText(getApplicationContext(), "noteID: " + noteID, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, NotesContentActivity.class);
        intent.putExtra("position", noteID);
        startActivity(intent);
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_read_room);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerViewAdapterForRoom = new RecyclerViewAdapterForRoom(new ArrayList<Notes>(), listener);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recyclerViewAdapterForRoom);
        viewModel = ViewModelProviders.of(this).get(NotesViewModel.class);
        viewModel.getNotesList().observe(GoReadRoomActivity.this, new Observer<List<Notes>>()
                //po wykonaniu tego wysypuje sie: viewModel = No such instance field 'viewModel'
                //edit: zmieniona implementacja na: android.arch.lifecycle:extensions:1.0.0-alpha9-1 w gradle i ruszylo
        {
            @Override
            public void onChanged(@Nullable List<Notes> notes) {
                recyclerViewAdapterForRoom.addNote(notes);
            }

        });


    }


    public static Notes getNotes(final AppDataBase dataBase, Notes notes) {
        dataBase.notesDAO().getall();
        return notes;
    }

   /* public void readNotesData() {

    }*/

    @Override
    protected void onDestroy() {
        AppDataBase.destroyInstance();
        super.onDestroy();
    }

    /*
    @Override
    // https://stackoverflow.com/questions/28296708/get-clicked-item-and-its-position-in-recyclerview/39707729 - sprawdz to
    //https://stackoverflow.com/questions/24885223/why-doesnt-recyclerview-have-onitemclicklistener
    public boolean onClick(View view) {
        Notes notes = (Notes) view.getTag();
        // tutaj cos zrobic, zeby zwrocic pozycje

      } */

    }



