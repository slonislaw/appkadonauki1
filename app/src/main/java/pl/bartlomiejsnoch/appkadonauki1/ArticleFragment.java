package pl.bartlomiejsnoch.appkadonauki1;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;


public class ArticleFragment extends Fragment {
    final static String ARG_POSITION = "position";
    int mCurrentPosition = -1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Jesli aktywnosc jest odnawiana (np po rotacji) ustawia to samo zaznaczenie
        //artykulu jak poprzednio - konieczne w widoku dwupanelowym
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
        }
        //Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_article, container, false);
    }
    @Override
    public void onStart() {
        super.onStart();
        //podczas startu sprawdza czy nie przekazano juz argumentu do fragmentu
        // jest to dobre miejsce, poniewaz ustanowiono juz layout wiec mozna bezpiczenie wywolac metode
        Bundle args = getArguments();
        if (args != null) {
            //ustaw artykul na podstawie przekazanego fragmentu
            updateArticleView(args.getInt(ARG_POSITION));
        } else if (mCurrentPosition != -1) {
            //ustawia artykul na bazie stanu zdefiniowanego w onCreateView
            updateArticleView(mCurrentPosition);
        }
    }

    public void updateArticleView(int position)

    {
        TextView article = getActivity().findViewById(R.id.article); //??????
        article.setText(Ipsum.Articles[position]);
        mCurrentPosition = position;
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //zapisuje obecnie zaznaczony artykul w razie koniecznosci ponownego odtworzenia zaznaczenia
        outState.putInt(ARG_POSITION, mCurrentPosition);
    }

}
