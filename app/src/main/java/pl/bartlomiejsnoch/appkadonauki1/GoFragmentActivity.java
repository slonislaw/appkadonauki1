package pl.bartlomiejsnoch.appkadonauki1;



import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;


//Komunikacja fragmentow odbywa sie za pomoca powiazanej aktywnosci
//fragmenty nie powinny komunikowac sie bezposrednio

public class GoFragmentActivity extends FragmentActivity
        implements HeadlinesFragment.OnHeadlineSelectedListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_fragment);
        //Sprawdza czy aktywnosc korzysta z wersji layoutu
        //ktora zawiera fragment_container:
        if (findViewById(R.id.fragment_container) != null) {
            //Jezeli przywracamy z poprzedniego stanu nie sprawdzamy
            //nie robimy nic i sotsuje sie return zeby uniknac nakladania fragmentow:
            if (savedInstanceState != null) {
                return;
            }
            //Tworzy nowy fragment do umieszczenia w layoucie aktywnosci:
            HeadlinesFragment firstFragment = new HeadlinesFragment();
            //W przypadku, gdy Aktywnosc wystartowala ze specjalnymi instrukcjami
            //z Intentu (polska nazwa?) przeazuje je do fragmentu jako argumenty
            firstFragment.setArguments(getIntent().getExtras());
            //Dodaje fragment do Frame_Layoutu 'fragment_container'
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, firstFragment).commit();
            //Ze wzgledu na to, ze fragment zostal dodany do kontenera FrameLayout w czasie wykonywania
            //zamiast definiowania go w layoucie jako <fragment>, moze go zastapic w widoku aktywnosci
            //innym fragmentem

        }
    }

    public void onArticleSelected(int position) {
        //uzytkownik wybral naglowek artykulu z HeadlinesFragment
        ArticleFragment articleFrag = (ArticleFragment)
                getSupportFragmentManager().findFragmentById(R.id.article_fragment);

        if (articleFrag != null) {
            //jezeli articleFrag jest dostepny jestesmy w widoku 2 panelowym (duzy layout)

            //wywoluje metode w ArticleFragment do zaktualizowania zawartosci:
            articleFrag.updateArticleView(position);


        } else {
            //Stworzenie fragmentu i przypisanie, ktory artykul ma pokazac
            ArticleFragment newFragment = new ArticleFragment();
            Bundle args = new Bundle();
            args.putInt(ArticleFragment.ARG_POSITION, position);
            newFragment.setArguments(args);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            //Zamienia zawartość fragment_container następnym fragmentem
            //i dodaje transakcje na stos, dzieki czmeu uzytkownik moze nawigowac z powrotem
            transaction.replace(R.id.fragment_container, newFragment);
            transaction.addToBackStack(null);

            //Przeprowadza transakcje
            transaction.commit();
        }
    }

}
