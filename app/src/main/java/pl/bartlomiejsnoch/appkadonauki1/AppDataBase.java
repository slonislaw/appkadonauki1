package pl.bartlomiejsnoch.appkadonauki1;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {Notes.class}, version = 1)

public abstract class AppDataBase extends RoomDatabase {
    private static AppDataBase INSTANCE; //metoda static przynalezy do klasy (jako szablonu), a nie do obiektu klasy

    public abstract NotesDAO notesDAO();

    public static AppDataBase getAppDataBase(Context context) { //z dowolnego punktu w programie moge wywolac tylko te instacje bazy danych
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    AppDataBase.class,
                    "notes-database")
                    .allowMainThreadQueries() //zezwolono na zapytania w glowym watku - nie robic tego i sprawdzic PersistenceBasicSample
                    .build();


        }
        return INSTANCE;
    }

    //public abstract NotesDAO titleModel();

    public static void destroyInstance() {
        INSTANCE = null;
    }
}