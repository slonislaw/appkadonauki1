package pl.bartlomiejsnoch.appkadonauki1;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

public class NotesContentActivity extends AppCompatActivity {

    private SingleNoteViewModel singleNoteViewModel;
    private RecyclerViewAdapterForNoteContent recyclerViewAdapter;
    private RecyclerView recyclerView;
    int position;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_content);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int position = extras.getInt("position");
            this.position = position;
        }

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_for_note);
        recyclerViewAdapter = new RecyclerViewAdapterForNoteContent(new ArrayList<Notes>());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recyclerViewAdapter);
        singleNoteViewModel = ViewModelProviders.of(this).get(SingleNoteViewModel.class);
       // singleNoteViewModel.getNoteContent(position).observe()(NotesContentActivity, this, new Observer<List<Notes>>);

        //singleNoteViewModel.getNoteContent(position);

    }
    @Override
    protected void onDestroy() {
        AppDataBase.destroyInstance();
        super.onDestroy();
    }
}
