package pl.bartlomiejsnoch.appkadonauki1;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.provider.ContactsContract.CommonDataKinds.Phone;

public class GoShareActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_share);

        Intent intent = getIntent();
        Uri getcontactdata = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        //Uri getcontactdata = (Uri) intent.getExtras().get(Intent.EXTRA_STREAM);
        //Uri getcontactdata = intent.getData();
        String[] numberprojection = {ContactsContract.CommonDataKinds.Phone.NUMBER};
        //String[] numberprojection = {ContactsContract.PhoneLookup.NORMALIZED_NUMBER };
        String path = getcontactdata.getPath();
        Cursor cursor = this.getContentResolver().query(getcontactdata, numberprojection, null, null, null);
        cursor.moveToFirst();
        int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        String number = cursor.getString(column);
        TextView textView = (TextView) findViewById(R.id.textView3);
        textView.setText(number);
    }

  /*  @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data){
         if (resultCode == RESULT_OK) {

            Intent intent = getIntent();
            Uri getcontactdata = intent.getData();
            String[] numberprojection = {ContactsContract.CommonDataKinds.Phone.NUMBER};
            Cursor cursor = getContentResolver().query(getcontactdata, numberprojection, null, null, null);
            cursor.moveToFirst();
            int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            String number = cursor.getString(column);
            TextView textView = (TextView) findViewById(R.id.textView3);
            textView.setText(number);
        }
    }*/
}
