package pl.bartlomiejsnoch.appkadonauki1;

import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;


import java.util.List;


public class GoInteractActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_interact);
    }

    public void startBrowser(View view) { //stworzenie Intentu
        Uri webpage = Uri.parse("http://www.android.com");
        Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);

        PackageManager packageManager = getPackageManager(); //sprawdza, czy istnieje paczka (appka) obsługująca ten Intent
        List<ResolveInfo> activities = packageManager.queryIntentActivities(webIntent, 0);
        boolean isIntentSafe = activities.size() > 0;
        if (isIntentSafe) {
            startActivity(webIntent);
        }
    }

    public void startChooser(View view) {
        Uri webpage = Uri.parse("http://www.android.com");
        Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
        String title = getResources().getString(R.string.chooser_title);
        Intent chooser = Intent.createChooser(webIntent, title);
        if (webIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(chooser);
        }

    }
    //ta część wywołuje alikację kontakty, zeby pobrac dane:
    static final int PICK_CONTACT_REQUEST = 1; //to jest request code
    public void startResult(View view) {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
        pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE); //pokazuje kontakty wyłącznie z nrem telefonu
        startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
    }
    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data){
        //sprawdza do ktorego request sie odnosimy:
        if (requestCode == PICK_CONTACT_REQUEST) {
                //sprawdza rezultat dzialania:
                if (resultCode == RESULT_OK) {
                    Uri contactUri = data.getData(); //pobiera Uri klikniętego kontaktu
                    String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER}; //potrzebujemy tylko wartości numeru

                    //Wykonuje zapytanie do kontaktu o pozycje z numerem:
                    Cursor cursor = getContentResolver().query(contactUri, projection, null, null, null);
                    cursor.moveToFirst();

                    //Pobiera numer telefonu z kolumny NUMBER:
                    int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    String number = cursor.getString(column);
                    TextView textView = (TextView) findViewById(R.id.textView5);
                    textView.setText(number);
                }
            }
        }
    }



