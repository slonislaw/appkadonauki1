package pl.bartlomiejsnoch.appkadonauki1;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class HeadlinesFragment extends ListFragment {
    OnHeadlineSelectedListener mCallback;
    //Kontener aktywnosci musi impelemtowac ten interfejs
    public interface OnHeadlineSelectedListener {
        public void onArticleSelected(int position);
    }
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // inny widok listy dla starszych urzadzen
        int layout = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
                android.R.layout.simple_list_item_activated_1 : android.R.layout.simple_list_item_1;
        //
        setListAdapter(new ArrayAdapter<String>(getActivity(), layout, Ipsum.Headlines));
    }
    @Override
    public void onStart(){
        super.onStart();
        //w widoku dwupanelowym ustawia podswietlenie wybranej pozycji z listy
        if (getFragmentManager().findFragmentById(R.id.article_fragment) != null) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        //Upewnia sie, ze aktywnosc kontenera zostala zaimplementowana zwrotnie
        //jesli nie to wyrzuca wyjatek
        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "must implement OnHeadlineSelectedListener");
        }
    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //wysyla zdarzenie do glownej aktywnosci
        mCallback.onArticleSelected(position);

        //ustawia pozycje jako zaznaczona zeby byla podswietlona w widoku dwupanelowym
        getListView().setItemChecked(position, true);
    }
}
