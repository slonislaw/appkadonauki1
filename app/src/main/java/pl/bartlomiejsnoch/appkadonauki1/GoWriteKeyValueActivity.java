package pl.bartlomiejsnoch.appkadonauki1;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.content.SharedPreferences;

public class GoWriteKeyValueActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_write_key_value);
    }
    public void EnterKeyValue(View view){
        EditText editText = findViewById(R.id.editText2);
        String StringKeyValue = editText.getText().toString(); //pobiera string z pola editText
        int IntKeyValue = Integer.parseInt(StringKeyValue); //konwertuje String na int
        SharedPreferences sharedPref = getSharedPreferences("PREF_NAME"/*nazwa dodana, zeby drugi fragment rozpoznal key value*/, Context.MODE_PRIVATE); //Key value w trybie prywatnym moze byc odczytane tylko przez moja appke
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("key", IntKeyValue);
        editor.commit();
    }


}
