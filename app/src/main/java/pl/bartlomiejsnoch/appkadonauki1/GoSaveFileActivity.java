package pl.bartlomiejsnoch.appkadonauki1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;


import java.io.File;
import java.io.IOException;



public class GoSaveFileActivity extends AppCompatActivity {
    private String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_save_file);
    }


    static final int REQUEST_IMAGE_CAPTURE = 1;
    public void goCameraSave(View view) {
        Intent startCameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        if (startCameraIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try { //try-catch - obsługa wyjątków
                    String state = Environment.getExternalStorageState();
                   if (Environment.MEDIA_MOUNTED.equals(state)){ //na emulatorze zawsze jest mounted - do ogarnięcia
                    photoFile = createImageFileEx();}
                else {
                    photoFile = createImageFileIn();
                }
            } catch (IOException ex) {
                //tu muszę dopisać obsługę wyjątku - do doczytania
            }
            if (photoFile != null) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    Uri photoUri = Uri.fromFile(photoFile); //target API<24, 24 i powyzej trzeba uzyc fileprovider, zeby miec content:// zamiast file://
                    startCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                    startActivityForResult(startCameraIntent, REQUEST_IMAGE_CAPTURE);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Uri photoUri = FileProvider.getUriForFile(this, "pl.bartlomiejsnoch.appkadonauki1.fileprovider", photoFile);
                    startCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                    startActivityForResult(startCameraIntent, REQUEST_IMAGE_CAPTURE);
                }

            } else {
                Snackbar mySnackbar = Snackbar.make(view, "No camera app :(((", 15);
                mySnackbar.show();
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                Bitmap myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
                ImageView imageView = (ImageView) findViewById(R.id.imageView);
                imageView.setImageBitmap(myBitmap);
            }
        }
    }

    private File createImageFileEx() throws IOException {
            File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File savedPhoto = File.createTempFile("savedPhoto", ".jpg", storageDir);
            mCurrentPhotoPath = savedPhoto.getAbsolutePath(); // zapisuje file: path dla mozliwosci wyswietlenia
            return savedPhoto;
    }
    private File createImageFileIn() throws IOException { //nie działa - pobiera folder w miejscu instalacji, czyli /data, nie ma w nim prawa zapisu
            //File rootDir = getDir(Environment.DIRECTORY_PICTURES, MODE_PRIVATE);
            File rootDir = new File(this.getFilesDir(), "Pictures");
            rootDir.mkdirs();
            File savedPhoto = File.createTempFile("savedPhoto", ".jpg", rootDir);
            //File savedPhoto = new File(getFilesDir(), "savedPhoto.jpg");
            mCurrentPhotoPath = savedPhoto.getAbsolutePath(); // zapisuje file: path dla mozliwosci wyswietlenia
            return savedPhoto;
    }



}


