package pl.bartlomiejsnoch.appkadonauki1;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import android.view.View;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bartek on 24.12.2017.
 */

public class RecyclerViewAdapterForNoteContent extends RecyclerView.Adapter<RecyclerViewAdapterForNoteContent.RecyclerViewHolderForNoteContent> {
    private List<Notes> notesList;

    public RecyclerViewAdapterForNoteContent (ArrayList<Notes> notesList){
        this.notesList = notesList;
    }


    @Override
    public RecyclerViewHolderForNoteContent onCreateViewHolder(ViewGroup parent, int ViewType) {
        //dopisać to
        return new RecyclerViewHolderForNoteContent(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_for_notes_content, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolderForNoteContent holder, int position){
        Notes notesModel = notesList.get(position);
        holder.titleTextView.setText(notesModel.getTitle());
        holder.contentTextView.setText(notesModel.getContent());
        holder.itemView.setTag(notesModel);
    }

    @Override
    public int getItemCount() {
        return notesList.size();
    }

    static class RecyclerViewHolderForNoteContent extends RecyclerView.ViewHolder{
        private TextView titleTextView;
        private TextView contentTextView;

        RecyclerViewHolderForNoteContent(View view) {
            super(view);
            titleTextView = (TextView) view.findViewById(R.id.titleTextView2);
            contentTextView = (TextView) view.findViewById(R.id.contentTextView);
        }
    }

}
